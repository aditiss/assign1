const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 8000;

// Middleware to parse JSON and URL-encoded data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Serve a simple HTML form for input
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Handle the form submission
app.post('/count', (req, res) => {
  const text = req.body.text || '';
  const wordCount = countWords(text);
  res.send(`Word count: ${wordCount}`);
});

// Function to count words in text
function countWords(text) {
  const words = text.trim().split(/\s+/);
  return words.length;
}

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
